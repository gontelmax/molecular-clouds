#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 29 10:16:58 2019

@author: gontelm
"""

import numpy as np
import matplotlib.pyplot as plt
#import imageio





def datamap(dataarray, p_name, p_units, colormap='viridis', p_origin='lower', posmin=0, posmax=1024, posres=50, p_vmin=None, p_vmax=None, savedir='./', grid=True):    
    """Function used to plot and save colormaps of quantities.
    
    Inputs :
        dataarray  -- 2D array containing the values of the quantity
        p_name     -- (string) name of the file (without extension) and title of the plot
        p_units    -- (string) units of the quantity
        colormap   -- (string) default 'jet' ; (for more colormaps, see  https://matplotlib.org/3.1.0/tutorials/colors/colormaps.html )
        p_origin   -- (string : 'upper' or 'lower') position of the origin of the axis (upper left or lower left only) ; default 'lower'
        posmin     -- minimum value of the axis (position axis) ; default 0
        posmax     -- maximum value of the axis (position axis) ; default 1024
        posres     -- interval between two consecutives graduations on axis (position axis) ; default 50
        p_vmin     -- minimum value of the colorbar ; default None
        p_vmax     -- maximum value of the colorbar ; default None
        savedir    -- (string) path where the histogram will be saved ; default './'
        grid       -- (boolean) True to add a grid to the plot ; default True
    
    """
    if colormap=='bwr':
        p_vmax=np.amax(np.abs(dataarray))
        plt.matshow(dataarray, fignum=None, origin=p_origin, cmap=colormap, vmin=-p_vmax, vmax=p_vmax)
    else:
        plt.matshow(dataarray, fignum=None, origin='lower', cmap=colormap, vmin=p_vmin, vmax=p_vmax)
    plt.xticks(np.arange(posmin, posmax, posres), np.arange(posmin, posmax, posres), rotation=90, fontsize='x-small')
    plt.yticks(np.arange(posmin, posmax, posres), np.arange(posmin, posmax, posres), fontsize='x-small')
    if grid==True:
        plt.grid(which='both', axis='both')
    plt.colorbar()
    plt.title(p_name+'_'+p_units, loc='center', position=(0.5,-0.2))
    plt.savefig(fname=savedir+p_name+'.jpg', dpi=200, quality=95, bbox_inches='tight')


#==============================================================================


def spectremap(dataarray, p_name, vaxis, savedir='./', grid=True, colormap='jet', p_colorvmin=None, p_colorvmax=None):   
    """Function used to plot and save position-velocity cut.
    
    Inputs :
        dataarray   -- 2D array containing the values of the quantity
        p_name      -- (string) name of the file (without extension) and title of the plot
        vaxis       -- 1D array, velocity axis of the spectra [km.s-1]
        savedir     -- (string) path where the histogram will be saved ; default './'
        grid        -- (boolean) True to add a grid to the plot ; default True
        colormap    -- (string) default 'jet' ; (for more colormaps, see  https://matplotlib.org/3.1.0/tutorials/colors/colormaps.html )
        p_colorvmin -- minimum value of the colorbar ; default None
        p_colorvmax -- maximum value of the colorbar ; default None
    
    """
    plt.matshow(dataarray, fignum=None, origin='upper', aspect='auto', cmap=colormap, vmin=p_colorvmin, vmax=p_colorvmax)   # 'lower' pour avoir l'origine en bas à gauche
    plt.colorbar()
    plt.title(p_name, loc='center', position=(0.5,-0.04))
    y_positions=np.arange(0, 1024, 20)
    plt.yticks(y_positions, y_positions)
    
    vx=np.zeros_like(vaxis)
    for i in range(len(vaxis)): vx[i]=round(vaxis[i],2)
    nx = vx.shape[0]
    no_labels = 9 # how many labels to see on axis x
    step_x = int(nx / (no_labels - 1)) # step between consecutive labels
    x_positions = np.arange(0,nx,step_x) # pixel count at label position
    x_labels = vx[::step_x] # labels you want to see
    plt.xticks(x_positions, x_labels, rotation=75, fontsize='medium')
    plt.xlabel('velocity [km.s-1]')
    if grid==True:
        plt.grid(which='both', axis='both')
    plt.savefig(fname=savedir+p_name+".jpg", dpi=200, quality=95, bbox_inches='tight')


#==============================================================================


def spectre(p_vabs, p_Inu, p_label, p_filename, savedir='./', p_ymax=None, grid=True):
    """Function used to plot and save simple emission spectra.
    
    Inputs :
        p_vabs      -- 1D array containing the velocity axis (horizontal axis of the plot)
        p_Inu       -- 1D array containing values of the spectre (intensities)
        p_title     -- (string) title of the plot
        p_filename  -- (string) name of the file (without extension)
        savedir     -- (string) path where the histogram will be saved ; default './'
        p_ymax      -- maximum value of the vertical axis (intensity). It enable to get a same vertical axis for a bunch of spectre, which is useful to compare them ; default 5e-6   [K]
        grid        -- (boolean) True to add a grid to the plot ; default True
        
    """
    fig1 = plt.figure()
    fig1.subplots_adjust(top=1)
    ax1 = fig1.add_subplot(111)
    ax1.set_ylabel('I(v)  [K]')
    ax1.set_xlabel('v  [km.s-1]')
    ax1.set_xlim(np.amin(p_vabs), np.amax(p_vabs))
    if p_ymax is not None:
        ax1.set_ylim(0, p_ymax*1.05)
    #ax1.set_title('...')
    
    ax1.plot(p_vabs, p_Inu, color='blue', lw=1, label=p_label)
    plt.legend(loc='upper right')
    if grid==True: plt.grid()
    plt.savefig(fname=savedir+p_filename+'.jpg', dpi=100, quality=45, bbox_inches='tight')


#==============================================================================
#
#
## Routine créant un gif à partir d'une série d'image 
## Le nom des image utilisées doit se finir par un nbr identifiant leur position dans la série (suivi de l'extension '.jpg')
## (par exemple : 'spectre_emission_colonne_x=643.jpg')
# # p_imagedir (string) : path of the directory where the image are and the gif will be
# # p_name (string) : name of the gif (must end by '.gif')
# # p_imagenbr (int) : nbr of image in the gif
# # p_imagename (string) : begining (part before the nbr element) of the filename of images used to create the gif 
#    
#def creating_gif(p_imagedir, p_name, p_imagenbr, p_imagename):
#    with imageio.get_writer(p_imagedir+p_name, mode='I') as writer:
#        for i in range(p_imagenbr):
#            filename=p_imagedir+p_imagename+str(i)+'.jpg'
#            image = imageio.imread(filename)
#            writer.append_data(image)
#
#
#==============================================================================


# Routine traçant et enregistrant le profil de densité p_col_dens de l'espèce p_spc selon les p_x
 # p_spc et p_x (string) : par exemple 'CO' et 'x1' respectivement
 # p_col_dens (array of float) : tableau contenant les densités calculées pour chaque colonne 
 # dim (int) : nombre de colonnes 
 # savedir (string) : chemin du dossier où sera enregistré le graphe

def column_density(p_spc, p_x, p_col_dens, p_dim=1024, savedir='./'):
    fig1 = plt.figure()
    fig1.subplots_adjust(top=1)
    ax1 = fig1.add_subplot(111)
    title=p_spc+"_column_density_"+p_x
    ax1.set_ylabel(p_spc+' column density [cm-2]')
    ax1.set_xlabel(p_x+' position')
    ax1.set_xlim(0, p_dim-1)
    ax1.set_title(title)
    ax1.plot(range(p_dim), p_col_dens, color='blue', lw=1, label=title)
    plt.grid()
    plt.savefig(fname=savedir+title+".jpg", dpi=200, quality=95, bbox_inches='tight')


#==============================================================================


def histogram(p_data, p_name, p_units, log=False, p_bins=100, savedir='./'):
    """Function used to plot and save histograms.
    
    Inputs :
        p_data   -- data 
        p_name   -- (string) name of the file (without extension) and title of the plot
        p_units  -- (string) units of the quantity
        log      -- (boolean) True to set the horizontal axis in log10 ; default False
        p_bins   -- number of bins of the histogram ; default 100
        savedir  -- (string) path where the histogram will be saved ; default './'
    
    """
    fig1 = plt.figure()
    fig1.subplots_adjust(top=1)
    ax1 = fig1.add_subplot(111)
    filename='histogram_'+p_name
    xtitle=p_name+'_'+p_units
    if log==True:
        p_data=np.log10(p_data)
        filename='histogram_log10_'+p_name
        xtitle='log10_'+p_name+'_'+p_units    
    ax1.set_title(filename+'_'+str(p_bins)+'_bins')
    ax1.set_xlabel(xtitle)
    p_data=p_data.flatten()
    plt.hist(p_data, bins=p_bins)
    plt.grid()
    plt.savefig(fname=savedir+filename+'.jpg', dpi=200, quality=95, bbox_inches='tight')    
    

#==============================================================================


def velocity_field(p_u1, p_u2, p_U2, p_interv=20, name='velocity_field', savedir='./', p_scale=40):
    """Function used to plot and save velocity vector field.
    
    Inputs :
        p_u1      -- 2D array containing the horizontal axis components
        p_u2      -- 2D array containing the vertical axis components
        p_U2      -- 2D array containing the squared norm of the vectors
        p_interv  -- (integer) interval between 2 points where a vector is drawn
        name      -- (string) name of the file (without extension)
        savedir   -- (string) path where the plot will be saved ; default './'
        p_scale   -- (float) smaller scale -> longer arrow ; default 40
        
    """
    dim=len(p_u1)
    nbrpts=int(dim/p_interv)+1
    grid=np.arange(0, dim, p_interv)
    u1_field=np.zeros((nbrpts, nbrpts))
    u2_field=np.zeros_like(u1_field)
    u_color=np.zeros_like(u1_field)
    
    for i in range(nbrpts):
        for j in range(nbrpts):
            x=grid[i]
            y=grid[j]
            u1_field[i,j]=p_u1[x,y]
            u2_field[i,j]=p_u2[x,y]
            u_color[i,j]=np.sqrt(p_U2[x,y])
    
    tickspos=np.arange(0, nbrpts-1, int(nbrpts/8))
    tickslab=np.zeros_like(tickspos)
    ticksint=grid[nbrpts-1]/(nbrpts-1)
    for i in range(len(tickspos)): tickslab[i]=str(int(tickspos[i]*ticksint))

    fig1 = plt.figure()
    fig1.subplots_adjust(top=1)
    ax1 = fig1.add_subplot(111)
    title='velocity field [km.s-1]'
    ax1.set_title(title)
    plt.xticks(tickspos, tickslab)
    plt.yticks(tickspos, tickslab)
    quiver = ax1.quiver(np.arange(nbrpts), np.arange(nbrpts), u1_field, u2_field, u_color, units='inches', scale=p_scale, cmap='jet')
    plt.colorbar(quiver)
    plt.savefig(fname=savedir+name+'.jpg', dpi=200, quality=95, bbox_inches='tight')


#==============================================================================
    
    
#def addline(losaxis, losnbr, filename, color='r', savedir='./'):
#    """fucntion used to add a vertical or horizontal line to a plot.
#    
#    Inputs :
#        losaxis   -- 
#        losnbr    -- 
#        filename  -- 
#        color     -- 
#        savedir   -- 
#        
#    """
#    if losaxis=='x1':
#        plt.axvline(losnbr, c=color)
#    if losaxis=='x2':
#        plt.axhline(losnbr, c=color)
#    plt.savefig(fname=savedir+filename+'.jpg', dpi=200, quality=95, bbox_inches='tight')
#
#    
#    

