#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 23 17:35:56 2019

@author: gontelm
"""


import numpy as np
import read_data_2D
import fct_spectre_2D
import plotting_2D
import matplotlib.pyplot as plt
import os




datapath='./t_300_yr/vtk/'
#savepath='./t_300_yr/threshold/'
#datapath='./t_1000_yr/vtk/'
#savepath='./t_1000_yr/threshold/'
#datapath='./t_3000_yr/vtk/'
#savepath='./t_3000_yr/threshold/'

savepath='./test/'


los='x1'   # 'x1' or 'x2'
losnbr=755   # between 0 and 1023 (included)


""" The thresholds values (and their labels) can be modified in line 123 (and 124) of the script """


# Which plots ?
plot_spectre_thr=True
plot_spectre_contrib=True
plot_nh=True
plot_nco_contrib=True
plot_nco_vel=True
plot_temper_press=True

grid=True






#####################################################
#####################################################






# Creation of a directory to save spectres and graphes
if os.path.exists(savepath) == False:
    os.mkdir(savepath)


addtoname=los+'_'+str(losnbr)+'_'


# Récupération des données : 
dat=read_data_2D.read_data(datapath)
nh=dat[0]
ntot=dat[1]
xco=dat[2]
nco=dat[3]
pth=dat[4]
tkin=dat[5]
u1=dat[6]
u2=dat[7]
U2=dat[8]
rho=dat[9]
pdyn=dat[10]


# Construction of the velocity axis 
u1min=np.amin(u1)                      #vitesse min selon l'axe des x1 [km.s-1]
u1max=np.amax(u1)                      #vitesse max selon l'axe des x1 [km.s-1]
vaxis_u1=fct_spectre_2D.vel_axis(u1)   # [m s-1] 
nbptsv_u1=len(vaxis_u1)

u2min=np.amin(u2)                      #vitesse min selon l'axe des x2 [km.s-1]
u2max=np.amax(u2)                      #vitesse max delon l'axe des x2 [km.s-1]
vaxis_u2=fct_spectre_2D.vel_axis(u2)    # [m s-1] 
nbptsv_u2=len(vaxis_u2)



if los=='x1':
    losb='x2'
    nh_col=nh[:,losnbr]
    xco_col=xco[:,losnbr]
    pth_col=pth[:,losnbr]
    pdyn_col=pdyn[:,losnbr]
    u_col=u2[:,losnbr]   # [km.s-1]
    ulab='u2'
    vaxis=vaxis_u2   # [m.s-1]
if los=='x2':
    losb='x1'
    nh_col=nh[losnbr]
    xco_col=xco[losnbr]
    pth_col=pth[losnbr]
    pdyn_col=pdyn[losnbr]
    u_col=u1[losnbr]   # [km.s-1]
    ulab='u1'
    vaxis=vaxis_u1   # [m.s-1]
nco_col=nh_col*xco_col
tkin_col=pth_col/(0.6*nh_col)

dim=len(nh_col)

nhmaxloc=np.amax(nh_col)        # max value of nh
nhmedloc=np.median(nh_col)      # mediane value of nh

print('n_H max = '+str(nhmaxloc)+' cm-3')
print('n_H median = '+str(nhmedloc)+' cm-3')



############################################

threshold=np.array([nhmaxloc, nhmaxloc*0.75, nhmaxloc*0.5, nhmedloc, nhmedloc*0.5])
thrlabel=['nH_max', 'nH_max*0.75', 'nH_max/2', 'nH_med', 'nH_med/2']

############################################


nbrthr=len(threshold)
  


spectre_thr=np.zeros((nbrthr, len(vaxis)))
nco_thr=np.zeros((nbrthr, dim))
#surf_thr=np.zeros(nbrthr)


for i in range(nbrthr):
    thr=threshold[i]
    if thr==nhmaxloc:
        spectre_thr[i]=fct_spectre_2D.spectre_column(los, losnbr, nco, tkin, u1, u2)
        nco_thr[i]=nco_col
    else:
        nhl=fct_spectre_2D.array_threshold(nh_col, thr, '<')
        ncol=nhl*xco_col
        nco_thr[i]=ncol
        tkinl=pth_col/(0.6*nhl)
        spectre_thr[i]=fct_spectre_2D.spectre_column_thr(ncol, tkinl, u_col*1e3, vaxis)    # u_col and vaxis in [m.s-1]
    #surf_thr[i]=np.trapz(spectre_thr[i], dx=resol)



#%%
    
    
if plot_spectre_thr==True:
    
    fig1 = plt.figure()                          # spectre for the different threshold
    fig1.subplots_adjust(top=1)
    ax1 = fig1.add_subplot(111)
    ax1.set_ylabel('I(v)  [K]')
    ax1.set_ylim(0, np.amax(spectre_thr)*1.05)
    ax1.set_xlabel('v  [km.s-1]')
    ax1.set_title('spectre in '+los+'='+str(losnbr)+' for different threshold on nH')
    if grid==True:
        ax1.grid(which='both', axis='x')
        ax1.grid(which='major', axis='y')
    ax1.minorticks_on()
    for i in range(nbrthr):
        ax1.plot(vaxis*1e-3, spectre_thr[i], lw=1.2, label='nH < '+thrlabel[i])
    plt.legend(loc='upper right')
    plt.savefig(fname=savepath+addtoname+'spectre_thresholds.jpg', dpi=200, quality=95, bbox_inches='tight')
    
    
    
if plot_spectre_contrib==True:    
    
    for i in range(nbrthr):
        if threshold[i]==nhmedloc:
            thr_med=i
    
    fig1 = plt.figure()                          # spectre and contribution of the values under/over the local mediane value of nh
    fig1.subplots_adjust(top=1)
    ax1 = fig1.add_subplot(111)
    ax1.set_ylabel('I(v)  [K]')
    ax1.set_ylim(0, np.amax(spectre_thr)*1.05)
    ax1.set_xlabel('v  [km.s-1]')
    ax1.set_title('spectre in '+los+'='+str(losnbr)+' - contribution over / under the median of nH')
    if grid==True:
        ax1.grid(which='both', axis='x')
        ax1.grid(which='major', axis='y')
    ax1.minorticks_on()
    ax1.plot(vaxis*1e-3, spectre_thr[0], lw=1.2, label='all')
    ax1.plot(vaxis*1e-3, spectre_thr[thr_med], lw=1.2, label='under nH_med')
    ax1.plot(vaxis*1e-3, spectre_thr[0]-spectre_thr[thr_med], lw=1.2, label='over nH_med')
    plt.legend(loc='upper right')
    plt.savefig(fname=savepath+addtoname+'spectre_contributions.jpg', dpi=200, quality=95, bbox_inches='tight')
    


#%%



if plot_nh==True:
    
    fig1 = plt.figure()                          # n_H along the los and med/mean values
    fig1.subplots_adjust(top=1)
    ax1 = fig1.add_subplot(111)
    ax1.set_xlim(0,1024)
    ax1.set_ylabel('n(H)  [cm-3]')
    ax1.set_ylim(0, nhmaxloc*1.05)
    ax1.set_xlabel(losb+'  position')
    ax1.set_title('n(H) along the los '+los+'='+str(losnbr))
    if grid==True:
        ax1.grid(which='both', axis='x')
        ax1.grid(which='major', axis='y')
    ax1.minorticks_on()
    ax1.plot(range(dim), nh_col, lw=1.2, label='n(H)')
    plt.axhline(nhmedloc, c='r', label='median')
    plt.axhline(nhmaxloc*0.5, c='b', label='max/2')
    plt.legend()
    plt.savefig(fname=savepath+addtoname+'n_H.jpg', dpi=200, quality=95, bbox_inches='tight')
    
    
    
if plot_nco_contrib==True:
    
    fig1 = plt.figure()                          # log10 nco along the los
    fig1.subplots_adjust(top=1)
    ax1 = fig1.add_subplot(111)
    ax1.set_xlim(0,1024)
    ax1.set_ylabel('log10 n(CO)  [cm-3]')
    ax1.set_xlabel(losb+'  position')
    ax1.set_title('log10 n(CO) along the los '+los+'='+str(losnbr))
    if grid==True:
        ax1.grid(which='both', axis='x')
        ax1.grid(which='major', axis='y')
    ax1.minorticks_on()
    ax1.plot(range(dim), np.log10(nco_thr[0]), lw=1.2, label='nH > '+thrlabel[2])
    ax1.plot(range(dim), np.log10(nco_thr[2]), lw=1.2, label=thrlabel[3]+' < nH < '+thrlabel[2], color='r')
    ax1.plot(range(dim), np.log10(nco_thr[3]), lw=1.2, label='nH < '+thrlabel[3], color='g')
    plt.legend()
    plt.savefig(fname=savepath+addtoname+'log10_n_CO_thresholds.jpg', dpi=200, quality=95, bbox_inches='tight')
    
    
    
#fig1 = plt.figure()                          # velocity along the los
#fig1.subplots_adjust(top=1)
#ax1 = fig1.add_subplot(111)
#ax1.set_xlim(0,1024)
#ax1.set_ylabel(ulab+'  [km.s-1]')
#ax1.set_ylim(np.amin(u_col)*1.05, np.amax(u_col)*1.05)
#ax1.set_xlabel(losb+'  position')
#ax1.set_title('velocity along the los '+los+'='+str(losnbr))
#ax1.grid(which='both', axis='both')
#ax1.minorticks_on()
#ax1.plot(range(dim), u_col, lw=1.5)
#plt.savefig(fname=savepath+addtoname+'velocity.jpg', dpi=200, quality=95, bbox_inches='tight')
    
    
    
if plot_nco_vel==True:
    
    fig1 = plt.figure()                          # velocity and log10 nco along the los
    fig1.subplots_adjust(top=1)
    ax1 = fig1.add_subplot(111)
    ax1.set_xlabel(losb+'  position')
    ax1.set_xlim(0,1024)
    ax1.set_ylabel(ulab+'  [km.s-1]')
    ax1.set_ylim(np.amin(u_col)*1.1, np.amax(u_col)*1.7)
    ax1.set_title('log10 n(CO) and velocity along the los '+los+'='+str(losnbr))
    if grid==True:
        ax1.grid(which='both', axis='x')
        #ax1.grid(which='both', axis='both')
    ax1.minorticks_on()
    ax1.plot(range(dim), u_col, lw=1.5, label='velocity '+ulab, color='b')
    
    ax2 = ax1.twinx()
    ax2.set_ylabel('log10 n(CO)  [cm-3]')
    #ax2.grid(which='both', axis='x')
    ax2.set_ylim(-7.4, -3.0)
    ax2.minorticks_on()
    ax2.plot(range(dim), np.log10(nco_thr[0]), lw=1.5, label='nH > '+thrlabel[2])
    ax2.plot(range(dim), np.log10(nco_thr[2]), lw=1.5, label=thrlabel[3]+' < nH < '+thrlabel[2], color='r')
    ax2.plot(range(dim), np.log10(nco_thr[3]), lw=1.5, label='nH < '+thrlabel[3], color='g')
    ax2.legend(loc='upper right')
    ax1.legend(loc='upper left')
    plt.savefig(fname=savepath+addtoname+'log10_n_CO_and_velocity.jpg', dpi=200, quality=95, bbox_inches='tight')
    


#%%



if plot_temper_press==True:
    
    fig1 = plt.figure()                          # temperature, thermal pressure and dynamical pressure along the los
    fig1.subplots_adjust(top=1)
    ax1 = fig1.add_subplot(111)
    ax1.set_xlabel(losb+'  position')
    ax1.set_xlim(0,1024)
    ax1.set_ylabel('log10 T_kin  [K]')
    #ax1.set_ylim(np.amin(u_col)*1.05, np.amax(u_col)*1.05)
    ax1.set_title('temperature and pressures along the los '+los+'='+str(losnbr))
    ax1.grid(which='both', axis='both')
    ax1.minorticks_on()
    ax1.plot(range(dim), np.log10(tkin_col), lw=1.5, label='T_kin', color='black')
    ax2 = ax1.twinx()
    ax2.set_ylabel('log10 P  [K.cm-3]')
    ax2.grid(which='both', axis='x')
    #ax2.grid(which='major', axis='y')
    ax2.minorticks_on()
    ax2.plot(range(dim), np.log10(pth_col), lw=1.3, label='P_th', color='g')
    ax2.plot(range(dim), np.log10(pdyn_col), lw=1.3, label='P_dyn', color='r')
    ax1.legend()
    ax2.legend()
    plt.savefig(fname=savepath+addtoname+'temperature_and_pressures.jpg', dpi=200, quality=95, bbox_inches='tight')



#%%


#%%

#
#fig1 = plt.figure()
#fig1.subplots_adjust(top=1)
#ax1 = fig1.add_subplot(111)
#ax1.set_xlabel('n_H threshold [cm-3]')
#ax1.set_ylabel('normalized surface under the spectre')
#ax1.set_title('Evolution of the surface under the spectre for different threshold on nH')
#ax1.grid()
#surf_thr=surf_thr/surf_thr[0]           #normalisation
#plt.plot(threshold, surf_thr, lw=1)
#plt.savefig(fname=savepath+addtoname+'surface_under_spectre.jpg', dpi=100, quality=45, bbox_inches='tight')



