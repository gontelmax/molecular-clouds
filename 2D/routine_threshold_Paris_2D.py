#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul  3 10:22:42 2019

@author: gontelm
"""

import numpy as np
import read_data_2D
import fct_spectre_2D
import plotting_2D
import matplotlib.pyplot as plt
import os




datapath='./t_300_yr/vtk/'
#savepath='./t_300_yr/threshold/'
#datapath='./t_1000_yr/vtk/'
#savepath='./t_1000_yr/threshold/'
#datapath='./t_3000_yr/vtk/'
#savepath='./t_3000_yr/threshold/'

savepath='./test/'


los='x1'   # 'x1' or 'x2'
losnbr=755   # between 0 and 1023 (included)


""" The thresholds values (and their labels) can be modified in line 123 (and 124) of the script """


plot_umasks=True
umasks=[-1.7, -0.1, 0.55, 1.6]
markers=['.', '+', 'd', '+']


# Which plots ?
plot_spectre_contrib=True
plot_nh=True
plot_nco_contrib=True



grid=True






#####################################################
#####################################################






# Creation of a directory to save spectres and graphes
if os.path.exists(savepath) == False:
    os.mkdir(savepath)


addtoname=los+'_'+str(losnbr)+'_'


# Récupération des données : 
dat=read_data_2D.read_data(datapath)
nh=dat[0]
ntot=dat[1]
xco=dat[2]
nco=dat[3]
pth=dat[4]
tkin=dat[5]
u1=dat[6]
u2=dat[7]
U2=dat[8]
rho=dat[9]
pdyn=dat[10]


# Construction of the velocity axis 
u1min=np.amin(u1)                      #vitesse min selon l'axe des x1 [km.s-1]
u1max=np.amax(u1)                      #vitesse max selon l'axe des x1 [km.s-1]
vaxis_u1=fct_spectre_2D.vel_axis(u1)   # [m s-1] 
nbptsv_u1=len(vaxis_u1)

u2min=np.amin(u2)                      #vitesse min selon l'axe des x2 [km.s-1]
u2max=np.amax(u2)                      #vitesse max delon l'axe des x2 [km.s-1]
vaxis_u2=fct_spectre_2D.vel_axis(u2)    # [m s-1] 
nbptsv_u2=len(vaxis_u2)

resol=fct_spectre_2D.resol


if los=='x1':
    losb='x2'
    nh_col=nh[:,losnbr]
    xco_col=xco[:,losnbr]
    pth_col=pth[:,losnbr]
    pdyn_col=pdyn[:,losnbr]
    u_col=u2[:,losnbr]   # [km.s-1]
    ulab='u2'
    vaxis=vaxis_u2   # [m.s-1]
if los=='x2':
    losb='x1'
    nh_col=nh[losnbr]
    xco_col=xco[losnbr]
    pth_col=pth[losnbr]
    pdyn_col=pdyn[losnbr]
    u_col=u1[losnbr]   # [km.s-1]
    ulab='u1'
    vaxis=vaxis_u1   # [m.s-1]
nco_col=nh_col*xco_col
tkin_col=pth_col/(0.6*nh_col)

dim=len(nh_col)

nhmaxloc=np.amax(nh_col)        # max value of nh
nhmedloc=np.median(nh_col)      # mediane value of nh

print('n_H max = '+str(nhmaxloc)+' cm-3')
print('n_H median = '+str(nhmedloc)+' cm-3')




#####################################

threshold=np.array([nhmaxloc, nhmaxloc*0.75, nhmaxloc*0.5, nhmedloc, nhmedloc*0.5])
thrlabel=['nH_max', 'nH_max*0.75', 'nH_max/2', 'nH_med', 'nH_med/2']

#####################################


nbrthr=len(threshold)
  




whmask=[]
xmasked=[]
compt=np.zeros(len(umasks))

def test_umasks(p_umasks, p_width, p_value):
    """
    
    """
    for i in range(len(p_umasks)):
        if p_value >= p_umasks[i]-p_width and p_value <= p_umasks[i]+p_width:
            return i


for i in range(dim):
    mask=test_umasks(umasks, 3*resol, u_col[i])
    if mask is not None:
        whmask.append(mask)
        xmasked.append(i)
        for j in range(len(umasks)):
            if mask == j: compt[j]=compt[j]+1
mask0=[[],[]]
mask1=[[],[]]
mask2=[[],[]]
mask3=[[],[]]
for i in range(len(whmask)):
    if whmask[i]==0: mask0[0].append(xmasked[i])
    if whmask[i]==1: mask1[0].append(xmasked[i])
    if whmask[i]==2: mask2[0].append(xmasked[i])
    if whmask[i]==3: mask3[0].append(xmasked[i])
for i in mask0[0]: mask0[1].append(nco_col[i])
for i in mask1[0]: mask1[1].append(nco_col[i])
for i in mask2[0]: mask2[1].append(nco_col[i])
for i in mask3[0]: mask3[1].append(nco_col[i])
   


spectre_thr=np.zeros((nbrthr, len(vaxis)))
nco_thr=np.zeros((nbrthr, dim))
#surf_thr=np.zeros(nbrthr)


for i in range(nbrthr):
    thr=threshold[i]
    if thr==nhmaxloc:
        spectre_thr[i]=fct_spectre_2D.spectre_column(los, losnbr, nco, tkin, u1, u2)
        nco_thr[i]=nco_col
    else:
        nhl=fct_spectre_2D.array_threshold(nh_col, thr, '<')
        ncol=nhl*xco_col
        nco_thr[i]=ncol
        tkinl=pth_col/(0.6*nhl)
        spectre_thr[i]=fct_spectre_2D.spectre_column_thr(ncol, tkinl, u_col*1e3, vaxis)    # u_col and vaxis in [m.s-1]
    #surf_thr[i]=np.trapz(spectre_thr[i], dx=resol)




#%%

if plot_spectre_contrib==True:    
    
    for i in range(nbrthr):
        if threshold[i]==nhmedloc:
            thr_med=i
    
    fig1 = plt.figure()                          # spectre and contribution of the values under/over the local mediane value of nh
    fig1.subplots_adjust(top=1)
    ax1 = fig1.add_subplot(111)
    ax1.set_ylabel('I(v)  [K]')
    ax1.set_ylim(0, np.amax(spectre_thr)*1.05)
    ax1.set_xlabel('v  [km.s-1]')
    ax1.set_title('spectre in '+los+'='+str(losnbr)+' - contribution over / under the median of nH')
    if grid==True:
        ax1.grid(which='both', axis='x')
        ax1.grid(which='major', axis='y')
    ax1.minorticks_on()
    ax1.plot(vaxis*1e-3, spectre_thr[0], lw=1.2, label='all')
    ax1.plot(vaxis*1e-3, spectre_thr[thr_med], lw=1.2, label='under nH_med')
    ax1.plot(vaxis*1e-3, spectre_thr[0]-spectre_thr[thr_med], lw=1.2, label='over nH_med')
    for i in range(len(umasks)):
        ax1.axvline(umasks[i], c='black')
    plt.legend(loc='upper right')
    plt.savefig(fname=savepath+addtoname+'spectre_contributions.jpg', dpi=200, quality=95, bbox_inches='tight')
    

#%%

if plot_nh==True:
    
    fig1 = plt.figure()                          # n_H along the los and med/mean values
    fig1.subplots_adjust(top=1)
    ax1 = fig1.add_subplot(111)
    ax1.set_xlim(0,1024)
    ax1.set_ylabel('n(H)  [cm-3]')
    ax1.set_ylim(0, nhmaxloc*1.05)
    ax1.set_xlabel(losb+'  position')
    ax1.set_title('n(H) along the los '+los+'='+str(losnbr))
    if grid==True:
        ax1.grid(which='both', axis='x')
        ax1.grid(which='major', axis='y')
    ax1.minorticks_on()
    ax1.plot(range(dim), nh_col, lw=1.2, label='n(H)')
    plt.axhline(nhmedloc, c='r', label='median')
    plt.axhline(nhmaxloc*0.5, c='b', label='max/2')
    plt.legend()
    plt.savefig(fname=savepath+addtoname+'n_H.jpg', dpi=200, quality=95, bbox_inches='tight')
    

#%%


if plot_nco_contrib==True:
    
    fig1 = plt.figure()                          # log10 nco along the los
    fig1.subplots_adjust(top=1)
    ax1 = fig1.add_subplot(111)
    ax1.set_xlim(0,1024)
    ax1.set_ylabel('log10 n(CO)  [cm-3]')
    ax1.set_xlabel(losb+'  position')
    ax1.set_title('log10 n(CO) along the los '+los+'='+str(losnbr))
    if grid==True:
        ax1.grid(which='both', axis='x')
        ax1.grid(which='major', axis='y')
    ax1.minorticks_on()
    ax1.plot(range(dim), np.log10(nco_thr[0]), lw=1.2, label='nH > '+thrlabel[2])
    ax1.plot(range(dim), np.log10(nco_thr[2]), lw=1.2, label=thrlabel[3]+' < nH < '+thrlabel[2], color='r')
    ax1.plot(range(dim), np.log10(nco_thr[3]), lw=1.2, label='nH < '+thrlabel[3], color='g')
    plt.legend()
    plt.savefig(fname=savepath+addtoname+'log10_n_CO_thresholds.jpg', dpi=200, quality=95, bbox_inches='tight')
    
    
#%%


fig1 = plt.figure()                          # velocity and log10 nco along the los with masks
fig1.subplots_adjust(top=1)
ax1 = fig1.add_subplot(111)
ax1.set_xlabel(losb+'  position')
ax1.set_xlim(0,1024)
ax1.set_ylabel(ulab+'  [km.s-1]')
ax1.set_ylim(np.amin(u_col)*1.1, np.amax(u_col)*1.7)
ax1.set_title('log10 n(CO) and velocity along the los '+los+'='+str(losnbr))
ax1.grid(which='both', axis='x')
#ax1.grid(which='both', axis='both')
ax1.minorticks_on()
ax1.plot(range(dim), u_col, lw=1.5, label='velocity '+ulab, color='b')
for i in range(len(umasks)):
    ax1.axhline(umasks[i], c='black')
ax2 = ax1.twinx()
ax2.set_ylabel('log10 n(CO)  [cm-3]')
#ax2.grid(which='both', axis='x')
ax2.set_ylim(-7.4, -3.0)
ax2.minorticks_on()
ax2.plot(range(dim), np.log10(nco_thr[0]), lw=1.3, label='nH > '+thrlabel[2])
ax2.plot(range(dim), np.log10(nco_thr[2]), lw=1.3, label=thrlabel[3]+' < nH < '+thrlabel[2], color='r')
ax2.plot(range(dim), np.log10(nco_thr[3]), lw=1.3, label='nH < '+thrlabel[3], color='g')
if plot_umasks==True:
    ax2.scatter(mask0[0], np.log10(mask0[1]), marker=markers[0], color='y', label='peak 1')
    ax2.scatter(mask1[0], np.log10(mask1[1]), marker=markers[1], color='y', label='peak 2')
    ax2.scatter(mask2[0], np.log10(mask2[1]), marker=markers[2], color='orange', label='peak 3')
    ax2.scatter(mask3[0], np.log10(mask3[1]), marker=markers[3], color='orange', label='peak 4')
ax2.legend(loc='upper right')
ax1.legend(loc='upper left')
if plot_umasks==True:
    plt.savefig(fname=savepath+addtoname+'log10_n_CO_and_velocity_masked.jpg', dpi=200, quality=95, bbox_inches='tight')
else:
    plt.savefig(fname=savepath+addtoname+'log10_n_CO_and_velocity.jpg', dpi=200, quality=95, bbox_inches='tight')




