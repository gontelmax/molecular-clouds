#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 28 14:44:53 2019

@author: gontelm
"""

from vtk import *
#import re
from numpy import shape,sum


def read_dat_vtk(filename):
    """Read vtk 2D files.
    
    Input : 
        filename -- (string) path and name of the vtk file (e.g. './t_300_yr/vtk/slic2d.nH.000010.vtk')
    
    Output : 
        dat -- dictionary containing the data array and his name
    """

    import vtk.util.numpy_support
    v2n=util.numpy_support.vtk_to_numpy
 
    Reader = vtkRectilinearGridReader()
    Reader.SetFileName(filename)
    Reader.SetReadAllScalars(True)
    Reader.Update()
    data=Reader.GetOutput()
    dim=data.GetDimensions()
    #print('dimensions:',dim)
    pd=data.GetPointData()
    dat={}
    for i in range(pd.GetNumberOfArrays()):
        name=pd.GetArrayName(i)
        dat[name]=v2n(data.GetPointData().GetArray(name)).reshape((dim[1],dim[0],dim[2]))
    return dat


# Read single variable in a vtk file
def read_single(file): 
    """Read vtk 2D file of a single variable.
    
    Input : 
        file -- (string) path and name of the vtk file (e.g. './t_300_yr/vtk/slic2d.nH.000010.vtk')
    
    Output : 
        a    -- 2D array containing the data from the vtk file (divided by 1024... ?)
    """

    # if re.search('M0',file):
    #     dat1=read_giorgos_vtk(file)
    # else:
    #     dat1=read_dat_vtk(file)
    dat1=read_dat_vtk(file)
    vars=dat1.keys()
    #print(vars)
    var=list(vars)[0]
    #print(var)
    a=dat1[var]
    #print(a)
    #print(shape(a))
    if shape(a)[2]>1:
       a=sum(a,axis=0)
    else:
       a=a[:,:,0]
    #print(a)
    #print(shape(a))
    # Normalises projection
    #print('Divides by ',shape(a)[0])
    a=a/shape(a)[0]#*float(thick)
    return a
