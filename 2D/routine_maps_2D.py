#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 29 16:51:04 2019

@author: gontelm
"""


import read_data_2D
import fct_spectre_2D
import numpy as np
import plotting_2D
import os



def maps_and_histo_2D(datapath, savepath, histo=True, maps=True, pgrid=False):
    """Function used to plot and save histograms and colormaps of the physical quantities.
    
    Inputs :
        datapath  -- (string) path of the repertory containing the .vtk data file of the variables (e.g. './t_300_yr/vtk/' )
        savepath  -- (string) path where histograms and colormaps will be saved (e.g. './t_300_yr/colormaps_and_histograms/' )
        histo     -- (boolean) True to compute histograms of the physical quantities ; default True
        maps      -- (boolean) True to compute colormaps of the physical quantities ; default True
        pgrid     -- (boolean) True to add a grid on the colormaps ; default False
    
    """
            
    # Creation of the directory where colormaps and histograms will be saved
    if os.path.exists(savepath) == False:
        os.mkdir(savepath)
    
    dat=read_data_2D.read_data(datapath)
    nh=dat[0]
    ntot=dat[1]
    xco=dat[2]
    nco=dat[3]
    pth=dat[4]
    tkin=dat[5]
    u1=dat[6]
    u2=dat[7]
    U2=dat[8]
    rho=dat[9]
    pdyn=dat[10]
    
    pop=fct_spectre_2D.population(nco, tkin)
    
    # Affichage et enregistrement des cartes couleurs des variables
    if maps==True:
        plotting_2D.datamap(np.log10(nh), "log10_n_H", "[cm-3]", 'jet', savedir=savepath, grid=pgrid)
        #plotting_2D.datamap(ntot, "n_tot", "[cm-3]", savedir=savepath, grid=pgrid)
        plotting_2D.datamap(np.log10(xco), "log10_x_CO", "[-]", 'jet', savedir=savepath, grid=pgrid)
        plotting_2D.datamap(np.log10(nco), "log10_n_CO", "[cm-3]", 'jet', savedir=savepath, grid=pgrid)
        plotting_2D.datamap(np.log10(pop), "log10_pop_CO", "[cm-3]", 'jet', savedir=savepath, grid=pgrid)
        plotting_2D.datamap(pth, "P_th", "[K.cm-3]", savedir=savepath, grid=pgrid)
        plotting_2D.datamap(pth, "P_th", "[K.cm-3]", 'jet', savedir=savepath, grid=pgrid)
        plotting_2D.datamap(tkin, "T_kin", "[K]", 'hot', savedir=savepath, grid=pgrid)
        plotting_2D.datamap(u1, "u1", "[km.s-1]", 'bwr', savedir=savepath, grid=pgrid)
        plotting_2D.datamap(u2, "u2", "[km.s-1]", 'bwr', savedir=savepath, grid=pgrid)
        plotting_2D.datamap(U2, "U_2", "[km2.s-2]", 'jet', savedir=savepath, grid=pgrid)
        #plotting_2D.datamap(rho, "rho", "[kg.cm-3]", 'jet', savedir=savepath, grid=pgrid)
        plotting_2D.datamap(pdyn, "P_dyn", "[K.cm-3]", 'jet', savedir=savepath, grid=pgrid)
        plotting_2D.datamap(pdyn, "P_dyn", "[K.cm-3]", savedir=savepath, grid=pgrid)
        
        plotting_2D.velocity_field(u1, u2, U2, 12, savedir=savepath)
    
    
    # Affichage et enregistrement des histogrammes des variables
    if histo==True:    
        plotting_2D.histogram(nh, "n_H", "[cm-3]", False, 100, savedir=savepath)
        plotting_2D.histogram(tkin, "T_kin", "[K]", False, 100, savedir=savepath)
        plotting_2D.histogram(nco, "n_CO", "[cm-3]", True, 100, savedir=savepath)
        plotting_2D.histogram(pop, "pop_CO", "[cm-3]", True, 100, savedir=savepath)
        plotting_2D.histogram(pth, "P_th", "[K.cm-3]", True, 100, savedir=savepath)
        plotting_2D.histogram(pdyn, "P_dyn", "[K.cm-3]", True, 100, savedir=savepath)
        plotting_2D.histogram(U2, "U_2", "[km.s-1]", True, 100, savedir=savepath)
    


#%%
    
    
    
    

datapath='./t_300_yr/vtk/'
savepath='./t_300_yr/colormaps_and_histograms/'
#datapath='./t_1000_yr/vtk/'
#savepath='./t_1000_yr/colormaps_and_histograms/'
#datapath='./t_3000_yr/vtk/'
#savepath='./t_3000_yr/colormaps_and_histograms/'

maps_and_histo_2D(datapath, savepath='./test/')

    
    
    
#%%
