#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 23 12:26:02 2019

@author: gontelm
"""

import read_data_2D
import fct_spectre_2D
import numpy as np
import plotting_2D
import os



def spectres_plot(datapath, savepath, spectremaps=True, spectresx1=[], spectresx2=[], pgrid=False, same_scale=True):
    """Function used to plot and save position-velocity maps and spectra.
    
    Inputs :
        datapath    -- (string) path of the repertory containing the .vtk data file of the variables (e.g. './t_300_yr/vtk/' )
        savepath    -- (string) path where histograms and colormaps will be saved (e.g. './t_300_yr/colormaps_and_histograms/' )
        spectremaps -- (boolean) True to compute histograms of the physical quantities ; default True
        spectresx1  -- 1D array containing the n° of the x1 columns for which you want to plot the spectra ; default []
        spectresx2  -- 1D array containing the n° of the x2 columns for which you want to plot the spectra ; default []
        pgrid       -- (boolean) True to add a grid on the colormaps ans spectra ; default False
        same_scale  -- (boolean) True to get the same intensity axis for the spectra (useful to compare them) ; default True
    
    """       
    # Creation of the directory where colormaps and histograms will be saved
    if os.path.exists(savepath) == False:
        os.mkdir(savepath)
    
    dat=read_data_2D.read_data(datapath)
#    nh=dat[0]
#    ntot=dat[1]
#    xco=dat[2]
    nco=dat[3]
#    pth=dat[4]
    tkin=dat[5]
    u1=dat[6]
    u2=dat[7]
#    U2=dat[8]
#    rho=dat[9]
#    pdyn=dat[10]


    # construction of the velocity axis
    vaxis_u1=fct_spectre_2D.vel_axis(u1)
    vaxis_u2=fct_spectre_2D.vel_axis(u2)

#    
#    dim=len(nh[0])

    
    Inu=fct_spectre_2D.spectre_array_tot(nco, tkin, u1, u2)
    Inux1=Inu[0]
    Inux2=Inu[1]
    
    
    if spectremaps==True:
        plotting_2D.spectremap(Inux1, 'map_spectres_x1', vaxis=vaxis_u2*1e-3, savedir=savepath, grid=pgrid)
        plotting_2D.spectremap(Inux2, 'map_spectres_x2', vaxis=vaxis_u1*1e-3, savedir=savepath, grid=pgrid)

    
    for i in spectresx1:
        addtoname='x1_'+str(i)+'_'
        if same_scale==True: ymax=np.amax(Inux1)
        else: ymax=None
        plotting_2D.spectre(vaxis_u2*1e-3, Inux1[i], 'spectre x1 = '+str(i), addtoname+'spectre', savedir=savepath, grid=pgrid, p_ymax=ymax)

    for i in spectresx2:
        addtoname='x2_'+str(i)+'_'
        if same_scale==True: ymax=np.amax(Inux2)
        else: ymax=None
        plotting_2D.spectre(vaxis_u1*1e-3, Inux2[i], 'spectre x2 = '+str(i), addtoname+'spectre', savedir=savepath, grid=pgrid, p_ymax=ymax)




#%%
        

datapath='./t_300_yr/vtk/'
savepath='./t_300_yr/spectres/'
#datapath='./t_1000_yr/vtk/'
#savepath='./t_1000_yr/spectres/'
#datapath='./t_3000_yr/vtk/'
#savepath='./t_3000_yr/spectres/'



spectres_plot(datapath, savepath='./test/', spectresx1=[250,450,755], spectresx2=[300,1000])


