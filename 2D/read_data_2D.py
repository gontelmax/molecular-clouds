#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  1 15:58:57 2019

@author: gontelm
"""

import utils_2D
import numpy as np
#import os




# Useful values
c_kB = 1.380649e-23      # Boltzmann constant [J.K-1]
uma = 1.661e-27          # atomic mass unit [kg]



def read_one(var, p_datapath):
    """Extract the 2D data array of a variable from a .vtk file.
    Inputs :
        var        -- (string) name of the variable you want the data
        p_datapath -- (string) path of the repertory containing the .vtk data file of the variable (e.g. './t_300_yr/vtk/' )
    
    Output :
        2D array containing the values of the quantity var (array transposed and multiplied by 1024... ?)
    
    """
    global snum
    if p_datapath=='./t_3000_yr/vtk/':
        snum='000100'
    if p_datapath=='./t_1000_yr/vtk/':
        snum='000036'
    if p_datapath=='./t_300_yr/vtk/':
        snum='000010'
    return utils_2D.read_single(p_datapath+'slic2d.'+var+'.'+snum+'.vtk').T*1024.


def read_data(p_datapath):
    """Read quantities and compute related quantities.
    
    Inputs :
        datapath -- (string) path of the repertory containing the .vtk data files (e.g. './t_300_yr/vtk/' ) 
    
    Outputs :
        nh   -- density [cm-3]
        ntot -- total density of particles  [cm-3]
        xco  -- CO abundancy [-]
        nco  -- CO number density [cm-3]
        pth  -- thermal pressure [K.cm-3]
        tkin -- kinetic temperature [K]
        u1   -- velocity along axis 1 (horizontal axis)  [km.s-1]
        u2   -- velocity along axis 2 (vertical axis)  [km.s-1]
        U2   -- velocity module  [km2.s-2]
        rho  -- mass density [kg.cm-3] 
        pdyn -- dynamic pressure [K.cm-3]

    """
    
    # Read physical quantities
    pth = read_one('p', p_datapath)            # thermal pressure [K.cm-3]
    nh  = read_one('nH', p_datapath)           # density of H [cm-3]
    u1  = read_one('u1', p_datapath)           # velocity axis 1 [km.s-1]
    u2  = read_one('u2', p_datapath)           # velocity axis 2 [km.s-1]
    xco = read_one('CO', p_datapath)             # CO abundancy [-]
    
    
    # Compute related quantities
    nco = xco*nh                   # CO number density [cm-3]
    ntot= 0.6*nh                   # total density of particles [cm-3]
    
    tkin = pth/ntot          # kinetic temperature [K]
    
    U2=u1**2+u2**2           # velocity module [km2.s-2]
    
    rho=nh*1.4*uma           # mass density [kg.cm-3]
    
    pdyn=rho*U2*1e6/c_kB    # dynamic pressure [K.cm-3]
    
    
    return (nh, ntot, xco, nco, pth, tkin, u1, u2, U2, rho, pdyn)
