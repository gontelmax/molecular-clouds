# -*- coding: utf-8 -*-
"""
Created on Fri Aug 23 03:07:44 2019

@author: Max
"""

import numpy as np
#import read_data_2D
#import plotting_2D
#import matplotlib.pyplot as plt
#import os




# Useful values
c_kB=1.380649e-23      # Boltzmann constant [J.K-1]
c_h=6.626070040e-34    # Planck constant [J.s]
c_c=2.99792458e8       # speed of light in vacuum [m.s-1]
uma = 1.661e-27        # atomic mass unit [kg]
pc_m = 3.086e16        # parsec [m]
m_CO=28*uma            # CO molecule mass [kg]
J_up=1                 # rotational quantum number J of the upper level of the transition
A_ul=7.4e-8            # spontaneous emission probability (or Einstein coeff) [s-1] of the CO(1-0) rotational line 
nu_rest=115.271203e9   # rest frequency [Hz] of the CO(1-0) rotational line
B_rot=nu_rest/2        # CO rotational constant ([s-1] or [Hz])
nrj = J_up*(J_up+1)*c_h*B_rot/c_kB # energy [K] of the level J_up 
gup = 2*J_up+1         # degeneracy of the level J_up
prefac = A_ul/(4*np.pi)*c_h*c_c/c_kB  #prefactor (K.sr-1.m.s-1)

resol=0.05   #résolution recherchée pour les canaux de vitesse [km.s-1]

L_scale= 1e14  # [m] soit 0.01 pc   (1pc = 3e16 m)





def vel_axis(p_u, res=resol):
    """Create the velocity axis of the spectre along a chosen direction axis.
    
    Inputs :
        p_u  -- 2D array containing the velocity component along the chosen direction axis [km.s-1]
        res  -- resolution of the velocity axis [km.s-1] ; default resol
        
    Output :
        v_axis -- 1D array containing the velocity axis along the chosen direction axis [m.s-1]
    """
    v_axis=np.arange(np.amin(p_u), np.amax(p_u)+res, step=res)*1e3
    return v_axis


#==============================================================================
    

def fct_partition(p_T):
    """Compute the partition function.
    
    Input :
        p_T  -- Temperature [K]
    
    Output :
        Q_partition  -- partition function value [-] 
        
    """           
    beta=c_h*B_rot/(c_kB*p_T)
    Q_partition=(np.power(beta, -1))+ 1/3 + beta/15   # approx for linear molecules
    return Q_partition




def spectre_1pt(p_ncoi, p_Ti, p_vi, p_vaxis):
    """Compute the spectre emitted in one point of the simulation.
    
    Inputs :
        p_ncoi   -- CO density [cm-3]
        p_Ti     -- temperature [K]
        p_vi     -- velocity component tangential to the axis of sight [m.s-1]
        p_vaxis  -- velocity axis of the spectre (depending on the axis of sight) [m.s-1]
    
    Output :
        contrib  -- 1D array containing the resulting spectre intensities [K.cm-3]
    
    """
    Q=fct_partition(p_Ti)
    sigmav2 = c_kB*p_Ti/m_CO
    population = p_ncoi*gup*np.exp(-nrj/p_Ti)/Q
    vprofile = np.exp(-(p_vaxis-p_vi)**2/2./sigmav2)/np.sqrt(2.*np.pi*sigmav2)
    contrib = prefac*population*vprofile
    return contrib

     


def spectre_column(p_x, p_losnbr, p_nco, p_tkin, u1, u2):  
    """Compute column spectre (spectre intensity integrated along a line of sight) of a given line of sight.
    
    Inputs :
        p_x      -- (string) 'x1' or 'x2', give on which of the axis the column is (e.g. a ''vertical'' column will be on the axis x1, the horizontal one)
        p_losnbr -- (integer) n° of the column from which the spectre is compute (e.g. ('x1', 531) will give the spectre of the column x1=531)
        p_nco    -- CO density (2D array) [cm-3]
        p_tkin   -- temperature (2D array) [K]
        u1    -- velocity component in the x1 axis (2D array) [km.s-1]
        u2    -- velocity component in the x2 axis (2D array) [km.s-1]
    
    Output :
        Inui  -- 1D array containing the column spectre intensities [K.cm-2]
    
    """
    if p_x=='x1':    # case x1 fixed : we look in the x2 axis direction so we need to take the u2 velocity to compute the spectra
        p_u=u2
    if p_x=='x2':    # case x2 fixed : we look in the x1 axis direction so we need to take the u1 velocity to compute the spectra
        p_u=u1
    vaxis=vel_axis(p_u)  # [m.s-1]
    jnui=np.zeros_like(vaxis)
    
    dim=len(p_nco[0])
    for z in range(dim):
        if p_x=='x1':                
            ncoi=p_nco[z,p_losnbr]
            Ti=p_tkin[z,p_losnbr]
            vi=p_u[z,p_losnbr]*1e3       # [m.s-1]
        
        if p_x=='x2':                
            ncoi=p_nco[p_losnbr, z]
            Ti=p_tkin[p_losnbr, z]
            vi=p_u[p_losnbr, z]*1e3      # [m.s-1]
        
        spect_loc=spectre_1pt(ncoi, Ti, vi, vaxis)
        jnui=np.ndarray.__add__(jnui, spect_loc)
    
    Inui=jnui*L_scale/dim
    return Inui




def spectre_array_tot(p_nco, p_tkin, p_u1, p_u2):    
    """Compute every column spectra along each of the two direction axis.
    
    Inputs :
        p_nco   -- CO density (2D array) [cm-3]
        p_tkin  -- temperature (2D array) [K]
        p_u1    -- velocity component in the x1 axis (2D array) [km.s-1]
        p_u2    -- velocity component in the x2 axis (2D array) [km.s-1]
    
    Output :
        Inux1  -- 2D array containing the column spectre intensities of x1-fixed columns [K.cm-2]
        Inux2  -- 2D array containing the column spectre intensities of x2-fixed columns [K.cm-2]
    
    """
    dim=len(p_nco[0])
    vaxis_u1=vel_axis(p_u1)   # [m.s-1]
    nbrptsv_u1=len(vaxis_u1)
    vaxis_u2=vel_axis(p_u2)   # [m.s-1]
    nbrptsv_u2=len(vaxis_u2)
    Inux1=np.zeros((dim, nbrptsv_u2))              
    Inux2=np.zeros((dim, nbrptsv_u1))              
    for losnbr in range(dim):
        Inux1[losnbr]=spectre_column('x1', losnbr, p_nco, p_tkin, p_u1, p_u2)
        Inux2[losnbr]=spectre_column('x2', losnbr, p_nco, p_tkin, p_u1, p_u2)
        if losnbr+1 in [200, 400, 600, 800, 1000, 1024]:
            print(str(losnbr+1)+" / "+str(dim))
    return(Inux1, Inux2)
  

#==============================================================================


def population(p_nco, p_T):
    """Compute the density of emitting CO.
    
    Inputs :
        p_nco -- CO density (2D array) [cm-3]
        p_T   -- temperature (2D array) [K]
    
    Output :
        popul -- density of emitting CO [cm-3]
    
    """
    Q=fct_partition(p_T)
    popul = p_nco*gup*np.exp(-nrj/p_T)/Q
    return popul


#==============================================================================
    

# Fct appliquant un seuil sur les valeurs d'un tableau, passant à 0 toutes celles en dessus du seuil
 # p_array (array 1D of float) : array 
 # p_threshold (float) : threshold to apply to the data
 # thrtype ('<' or '>') : if '<' then we keep the array's values that are < to p_threshold, else we keep the values > to p_threshold
  # newarray : same array than p_array but with 0 where the values of p_array were > to p_threshold
def array_threshold(p_array, p_threshold, thrtype='<'):
    """Apply a threshold on an array and set to 0 every value which don't fulfill the condition.
    
    Inputs :
        p_array      -- array on which applied the threshold
        p_threshold  -- value of the threshold
        thrtype      -- (string) '<' or '>', give the type of the condition used. If '<' then only the value inferior to p_threshold are preserved ; default '<'
    
    Output :
        newarray  -- array with threshold applied
    
    """
    newarray=np.zeros_like(p_array)
    
    if len(np.shape(p_array))==1:
        for i in range(len(p_array)):
            if thrtype=='>':
                if p_array[i]>p_threshold:
                    newarray[i]=p_array[i]
            if thrtype=='<':
                if p_array[i]<p_threshold:
                    newarray[i]=p_array[i]
    
    if len(np.shape(p_array))==2:
        for i in range(len(p_array[:,0])):
            for j in range(len(p_array[0,:])):
                if thrtype=='>':
                    if p_array[i,j]>p_threshold:
                        newarray[i,j]=p_array[i,j]
                if thrtype=='<':
                    if p_array[i,j]<p_threshold:
                        newarray[i,j]=p_array[i,j]
    
    return newarray
    

#==============================================================================
    

def dens_col(p_array, axis, L_scale):
    """Compute the column densities of an array along a chosen axis.
    
    Inputs :
        p_array  -- 2D array containing the density
        axis     -- (string) 'x1' or 'x2', give the axis of sight (e.g. if 'x1' then the colum densities are computed for the x1-fixed columns)
        L_scale  -- caracteristic length of the simulation
    
    Output :
        newarray  -- 1D array containing the column densities
    
    """
    dim=len(p_array[0])
    newarray=np.zeros(dim)
    if axis=='x1':
        for i in range(dim):
            newarray[i]=np.sum(p_array[:,i])*L_scale*1e2/dim
    if axis=='x2':
        for i in range(dim):
            newarray[i]=np.sum(p_array[i])*L_scale*1e2/dim
    
    return newarray


#==============================================================================
    

def spectre_column_thr(p_nco_col, p_tkin_col, p_u_col, vaxis):  
    """Compute column spectre directly from column array.
    
    Inputs :
        p_nco_col  -- CO density along the column (1D array) [cm-3]
        p_tkin_col -- temperature along the column (1D array) [K]
        p_u_col    -- velocity component (tangential to the column) along the column (1D array) [m.s-1]
        vaxis      -- velocity axis [m.s-1]
    
    Output :
        Inui  -- 1D array containing the column spectre intensities [K.cm-2]
    
    """

    jnui=np.zeros_like(vaxis)
    
    dim=len(p_nco_col)
    for z in range(dim):
        ncoi=p_nco_col[z]
        Ti=p_tkin_col[z]
        vi=p_u_col[z]
        
        spect_loc=spectre_1pt(ncoi, Ti, vi, vaxis)
        jnui=np.ndarray.__add__(jnui, spect_loc)
    
    Inui=jnui*L_scale/dim
    return Inui


