#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 29 16:51:04 2019

@author: gontelm
"""

import utils_3D
import numpy as np
#import plotting_3D
#import os




# Useful values
c_kB = 1.380649e-23      # Boltzmann constant [J.K-1]
uma = 1.661e-27          # atomic mass unit [kg]
pc_cm = 3.086e18         # parsec in [cm]


# Scaling
nh_scale   = 300.        # [cm-3]
L_scale    = 0.6*pc_cm   # [cm]     
T_scale    = 30.         # [K]    isothermal simulation so T=T_scale constant
Mach = 4                 # Mach number [-]
MachA = 1                # Alfvenic Mach number [-]

rho_scale = nh_scale*1.4*uma*1e3                          # [g.cm-3]
v_scale = Mach*np.sqrt(c_kB*T_scale/(2.3*uma))*1e-3       # [km.s-1]
B_scale = np.sqrt(np.pi*4*rho_scale)*v_scale*1e5/MachA    # [Gauss]
#t_scale    = L_scale*1e5/v_scale                          # timescale [s]



def read_data(struct, datapath='./vtk/'):
    """Read and scale quantities + compute related quantities
    
    Inputs :
        struct   -- (string) name of the structure you want the data
        datapath -- (string) path of the repertory containing the .vtk data file of the structure ; default './vtk/' 
    
    Outputs :
        rho  -- mass density [g.cm-3] 
        nh   -- density [cm-3]
        ntot -- total density of particles  [cm-3]
        vx   -- velocity along x  [km.s-1]
        vy   -- velocity along y  [km.s-1]
        vz   -- velocity along z  [km.s-1]
        v2   -- velocity module  [km2.s-2]
        Bx   -- magnetic field along x [microGauss]
        By   -- magnetic field along y [microGauss]
        Bz   -- magnetic field along z [microGauss]
        B2   -- magnetic field module  [microGauss2]
        pth  -- thermal pressure [K.cm-3]
        pdyn -- dynamic pressure [K.cm-3]
        pmag -- magnetic pressure [K.cm-3]
    
    """
    
    filename=datapath+struct+'.vtk'
    data=utils_3D.read_dat_vtk(filename)

    
    # Read and scale physical quantities
    rho = data['rho']*rho_scale               #  mass density [g.cm-3]     
    rhovx  = data['rvx']*rho_scale*v_scale    #  [g.cm-3.km.s-1]   
    rhovy  = data['rvy']*rho_scale*v_scale    #  [g.cm-3.km.s-1]
    rhovz  = data['rvz']*rho_scale*v_scale    #  [g.cm-3.km.s-1]
    dis = data['Dis']
    Bx = data['B_x']*B_scale*1e6              #  magnetic field along x [microGauss]
    By = data['B_y']*B_scale*1e6              #  magnetic field along y [microGauss]
    Bz = data['B_z']*B_scale*1e6              #  magnetic field along z [microGauss]
    
    nh = rho/(1.4*uma*1e3)          # density [cm-3]
    ntot = 0.6*nh                   # total density of particles  [cm-3]
    vx = rhovx/rho                  # velocity along x  [km.s-1]
    vy = rhovy/rho                  # velocity along y  [km.s-1]
    vz = rhovz/rho                  # velocity along z  [km.s-1]
    v2 = vx**2 + vy**2 + vz**2      # velocity module  [km2.s-2]
    B2 = Bx**2 + By**2 + Bz**2      # magnetic field module  [microGauss2]
    
    pth = ntot*T_scale                         # Thermal pressure [K.cm-3]
    pdyn = rho*v2/c_kB*1e3                     # Dynamic pressure [K.cm-3]
    pmag = B2*1e-12/(8*np.pi*c_kB)*1e-7        # Magnetic pressure [K.cm-3]
#    ptot = pth+pmag

        
    return (rho, nh, ntot, vx, vy, vz, v2, Bx, By, Bz, B2, pth, pdyn, pmag)



#def prescription_CO(p_nh, p_pmag, nhmin, pmagmax):
def prescription_CO(p_nh, nhmin, p_xco):
    """Compute CO density based on the prescription
    
    Inputs:
        p_nh  -- density [cm-3]
        nhmin -- threshold : minimum value of the density [cm-3]
        p_xco -- abundance of CO when the prescription is fulfilled [-]
    
    Output :
        nco -- density of CO [cm-3]
    """
    nco=np.zeros_like(p_nh)
    dim=len(p_nh)
    for i in range(dim):
        for j in range(dim):
            for k in range(dim):
                #if p_nh[i,j,k]>nhmin and p_pmag[i,j,k]<pmagmax:
                if p_nh[i,j,k]>nhmin :
                    nco[i,j,k]=p_nh[i,j,k]*p_xco
    return nco




