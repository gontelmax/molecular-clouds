#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug  2 10:33:13 2019

@author: gontelm
"""

import numpy as np
#import matplotlib.pyplot as plt
import os
#import utils_3D
#import plotting_3D
import read_data_3D
from astropy.io import fits




# Useful values
c_kB=1.380649e-23      # Boltzmann constant [J.K-1]
c_h=6.626070040e-34    # Planck constant [J.s]
c_c=2.99792458e8       # speed of light in vacuum [m.s-1]
uma = 1.661e-27        # atomic mass unit [kg]
pc_m = 3.086e16        # parsec [m]
m_CO=28*uma            # CO molecule mass [kg]
J_up=1                 # rotational quantum number J of the upper level of the transition
A_ul=7.4e-8            # spontaneous emission probability (or Einstein coeff) [s-1] of the CO(1-0) rotational line 
nu_rest=115.271203e9   # rest frequency [Hz] of the CO(1-0) rotational line
B_rot=nu_rest/2        # CO rotational constant ([s-1] or [Hz])
nrj = J_up*(J_up+1)*c_h*B_rot/c_kB # energy [K] of the level J_up 
gup = 2*J_up+1         # degeneracy of the level J_up

Temp=read_data_3D.T_scale     # Temperature [K], constant because isothermal simulation
betapart=c_h*B_rot/(c_kB*Temp)
Q_part=(np.power(betapart, -1))+ 1/3 + betapart/15     # Partition fct, approximation for linear molecules (like CO)
sigma2=c_kB*Temp/m_CO     # Standard deviation [m2.s-2] of velocity profile, due to thermal broadening

prefac = gup*np.exp(-nrj/Temp)/Q_part*A_ul/(4*np.pi)*c_h*c_c/c_kB  # prefactor [K.sr-1.m.s-1]

L_scale=read_data_3D.L_scale*1e-2   # [m]




def save_in_fits(p_data, axis1, axis2, axis3, filename, savepath='./'):
    """Save 3D array of data to file in .fits format.
    
    Inputs :
        p_data    -- 3D array containing the data to save 
        axis1     -- array containing the values of the first axis
        axis2     -- array containing the values of the second axis
        axis3     -- array containing the values of the third axis
        filename  -- (string) for example 'name.fits'
        savepath  -- where to save the file (default './')
        
    """    
    header = fits.getheader('template.fits', 0)
    
    # Update header
    #sec = 1./206265
    arcsec_to_deg=1./3600
    pix_to_arcsec=10  # here 1 pixel = 10 "
    header['OBJECT'] = 'CURRENT_SHEET'
    header['NAXIS1'] = len(axis1)
    header['NAXIS2'] = len(axis2)
    header['NAXIS3'] = len(axis3)
    header['CDELT1'] = (axis1[1]-axis1[0])*pix_to_arcsec*arcsec_to_deg  # the increment, in DEGREES
    header['CRPIX1'] = 0                        # the pixel containing the reference value
    header['CDELT2'] = (axis2[1]-axis2[0])*pix_to_arcsec*arcsec_to_deg  
    header['CRPIX2'] = 0
    header['CRVAL3'] = axis3[0]             # m/s
    header['CDELT3'] = (axis3[1]-axis3[0])  # m/s
    header['CRPIX3'] = 0
    header['ORIGIN'] = 'routine_spectre_3D.py'
    
    fits.writeto(savepath+filename, p_data, header, overwrite=True) #, format='fits')




def spectre_1pt(p_ncoi, p_ui, p_vaxisi):
    """Compute CO emission.
    
    Inputs :
        p_ncoi    -- CO density [cm-3]
        p_ui      -- velocity along the line of sight [m.s-1]
        p_vaxisi  -- one of the velocity axis value [m.s-1]
    
    Output :
        contrib   -- intensity of the spectrum at the p_vaxisi velocity value [K]
    
    """
    vprofile = np.exp(-(p_vaxisi-p_ui)**2/2./sigma2)/np.sqrt(2.*np.pi*sigma2)
    contrib = prefac*p_ncoi*vprofile
    return contrib




def spectra_data(struct, nhmin, xco, savedir='./', posmin=-30, posmax=30, vmin=-1., vmax=1., vres=0.05): 
    """Compute CO emission spectra along the 3 axis of data cubes and save them as .fits files.
    
    Inputs :
        struct   -- (string) name of the structure, must be the same than the .vtk file (e.g. 'shock_slow' for the 'shock_slow.vtk')
        nhmin    -- threshold : minimum value of the density [cm-3]
        xco      -- abundance of CO when the prescription is fulfilled [-]
        savedir  -- (string) path where to save the .fits spectra data files ; default './'
        posmin   -- minimum position value on any position axis ; default -30
        posmax   -- maximum position value on any position axis ; default 30
        vmin     -- minimum value for the velocity axis of the spectra ; default -1.    [km.s-1]
        vmax     -- maximum value for the velocity axis of the spectra ; default 1.     [km.s-1]
        vres     -- resolution of the velocity axis of the spectra ; default 0.05       [km.s-1]
        
    Outputs :
        dat      -- list of principal physical quantities data array (cf. read_data function documentation)
        spectra  -- array containing the spectra data along the 3 axis
        
    """
    
    # Creation of a directory to save the .fits files
    filepath=savedir+struct+'/'
    if os.path.exists(filepath) == False:
        os.mkdir(filepath)
    
    # Velocity axis
    vaxis=np.arange(vmin, vmax+vres, vres)*1e3   # [m.s-1] velocity axis for the spectra
    nbrptsv=len(vaxis)
    
    # Position axis
    xaxis=np.arange(posmin, posmax+1, 1)
    yaxis=xaxis
    zaxis=xaxis
    nbrptspos=len(xaxis)
    xaxis_pc=xaxis*L_scale/pc_m/(nbrptspos-1)
    xaxis_arcsec=xaxis_pc*1000     # because 1 pixel = 10"
    
    
    # Recuperation of the data of the simulation
    dat=read_data_3D.read_data(struct)
#    rho=dat[0]
    nh=dat[1]
#    ntot=dat[2]
    vx=dat[3]
    vy=dat[4]
    vz=dat[5]
#    v2=dat[6]
#    Bx=dat[7]
#    By=dat[8]
#    Bz=dat[9]
#    B2=dat[10]
#    pth=dat[11]
#    pdyn=dat[12]
#    pmag=dat[13]
#    ptot=dat[14]
    
    # Prescription for the CO painting
    nco=read_data_3D.prescription_CO(nh, nhmin, xco)
    
    
    # Creation of arrays for spectra's data
    spectrex = np.zeros((nbrptsv,nbrptspos,nbrptspos))
    spectrey = np.zeros_like(spectrex)
    spectrez = np.zeros_like(spectrex)
    
    
    # Spectra along axis 'x' :
    for i in range(nbrptsv):
        vaxisi=vaxis[i]
        for x in range(nbrptspos):
            ncoi=nco[:,:,x]
            ui=vx[:,:,x]*1e3   # in m.s-1
            spectrex[i]=spectrex[i]+spectre_1pt(ncoi, ui, vaxisi)
        spectrex[i]=spectrex[i]*L_scale/nbrptspos
    
    save_in_fits(spectrex, yaxis, zaxis, vaxis, 'spectres_along_x.fits', filepath)
    
    
    
    # Spectra along axis 'y' :
    for i in range(nbrptsv):
        vaxisi=vaxis[i]
        for y in range(nbrptspos):
            ncoi=nco[:,y,:]
            ui=vy[:,y,:]*1e3   # in m.s-1
            spectrey[i]=spectrey[i]+spectre_1pt(ncoi, ui, vaxisi)
        spectrey[i]=spectrey[i]*L_scale/nbrptspos
    
    save_in_fits(spectrey, xaxis, zaxis, vaxis, 'spectres_along_y.fits', filepath)
    
    
    
    # Spectra along axis 'z' :
    for i in range(nbrptsv):
        vaxisi=vaxis[i]
        for z in range(nbrptspos):
            ncoi=nco[z]    
            ui=vz[z]*1e3   # in m.s-1
            spectrez[i]=spectrez[i]+spectre_1pt(ncoi, ui, vaxisi)
        spectrez[i]=spectrez[i]*L_scale/nbrptspos
    
    save_in_fits(spectrez, xaxis, yaxis, vaxis, 'spectres_along_z.fits', filepath)

    
    
    spectra=np.array([spectrex, spectrey, spectrez])
    return (dat, spectra)




#%%




struct='current_sheet' 
#struct='shock_slow'
#struct='shock_fast'

# Prescription thresholds for the CO painting
xco=1e-4
nhmin=300   # [cm-3] prescription threshold on n_H

spectra_data(struct, nhmin, xco, savedir='./test/')





#%%





#rho=dat[0]
#nh=dat[1]
#ntot=dat[2]
#vx=dat[3]
#vy=dat[4]
#vz=dat[5]
#v2=dat[6]
#Bx=dat[7]
#By=dat[8]
#Bz=dat[9]
#B2=dat[10]
#pth=dat[11]
#pdyn=dat[12]
#pmag=dat[13]
#ptot=dat[14]


#nhmoy=np.mean(nh)
#nhmed=np.median(nh)
#ncomoy=np.mean(nco)
#ncomed=np.median(nco)
#pmagmoy=np.mean(pmag)
#pmagmed=np.median(pmag)
#ptotmoy=np.mean(ptot)
#ptotmed=np.median(ptot)



#Inumax=np.amax(spectrex)
#title='spectre in position y=29 and z=30 of the current sheet structure'
#plotting_3D.spectre(vaxis*1e-3, spectrex[:,30,29], title, 'spectre_y29_z30', filepath, Inumax)
#
#plotting_3D.spectremap(spectrex[:,:,29], 'map_spectre_along_x_axis_for_y29', 'z', vmin, vmax, resol, filepath)
#
#plotting_3D.spectremap(spectrex[:,30,:], 'map_spectre_along_x_axis_for_z30', 'y', vmin, vmax, resol, filepath)



