#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 29 10:16:58 2019

@author: gontelm
"""

import numpy as np
import matplotlib.pyplot as plt





def datamap(dataarray, p_name, p_units, xlab, ylab, colormap='jet', p_origin='lower', posmin=-30, posmax=30, posres=10, p_vmin=None, p_vmax=None, savedir='./', grid=True):
    """Function used to plot and save colormaps of quantities.
    
    Inputs :
        dataarray  -- 2D array containing the values of the quantity
        p_name     -- (string) name of the file (without extension) and title of the plot
        p_units    -- (string) units of the quantity
        xlab       -- (string) label of the horizontal axis
        ylab       -- (string) label of the vertical axis
        colormap   -- (string) default 'jet' ; (for more colormaps, see  https://matplotlib.org/3.1.0/tutorials/colors/colormaps.html )
        p_origin   -- (string : 'upper' or 'lower') position of the origin of the axis (upper left or lower left only) ; default 'lower'
        posmin     -- minimum value of the axis (position axis) ; default -30
        posmax     -- maximum value of the axis (position axis) ; default 30
        posres     -- interval between two consecutives graduations on the axis (position axis) ; default 10
        p_vmin     -- minimum value of the colorbar ; default None
        p_vmax     -- maximum value of the colorbar ; default None
        savedir    -- (string) path where the histogram will be saved ; default './'
        grid       -- (boolean) True to add a grid to the plot ; default True
    
    """
    plt.matshow(dataarray, fignum=None, origin=p_origin, cmap=colormap, vmin=p_vmin, vmax=p_vmax)   # 'lower' pour avoir l'origine en bas à gauche
    plt.xticks(np.arange(0, abs(posmin)+abs(posmax)+1, posres), np.arange(posmin, posmax+1, posres), rotation=90, fontsize='x-small')
    plt.yticks(np.arange(0, abs(posmin)+abs(posmax)+1, posres), np.arange(posmin, posmax+1, posres), fontsize='x-small')
    plt.xlabel(xlab)
    plt.ylabel(ylab)
    if grid==True:
        plt.grid(which='both', axis='both')
    plt.colorbar()
    plt.title(p_name+'_'+p_units, loc='center', position=(0.5,-0.2))
    plt.savefig(fname=savedir+p_name+'.jpg', dpi=200, quality=95, bbox_inches='tight')


#==============================================================================


def spectremap(dataarray, p_name, p_axis, posmin=-30, posmax=30, posres=10, vmin=-1.0, vmax=1.0, vres=0.25, savedir='./', colormap='jet', p_origin='lower', grid=True):
    """Function used to plot and save colormaps of spectra (or position-velocity cuts).
    
    Inputs :
        dataarray  -- 2D array containing the data of the spectra (intensities values of each spectre)
        p_name     -- (string) name of the file (without extension) and title of the plot
        p_axis     -- (string) which axis ('x', 'y' or 'z') is on the horizontal axis of the plot. (Example : for a pos-vel cut along y axis with z=-12 fixed, the horizontal axis is x axis)  
        posmin     -- minimum value of the horizontal axis (position axis) ; default -30
        posmax     -- maximum value of the horizontal axis (position axis) ; default 30
        posres     -- interval between two consecutives graduations on the horiz axis (position axis) ; default 10
        vmin       -- minimum value of the vertical axis (velocity axis) ; default -1.0   [km.s-1]
        vmax       -- maximum value of the vertical axis (velocity axis) ; default 1.0    [km.s-1]
        vres       -- interval between two consecutives graduations on the vertic axis (velocity axis) ; default 0.25   [km.s-1]
        savedir    -- (string) path where the histogram will be saved ; default './'
        colormap   -- (string) default 'jet' ; (for more colormaps, see  https://matplotlib.org/3.1.0/tutorials/colors/colormaps.html )
        p_origin   -- (string : 'upper' or 'lower') position of the origin of the axis (upper left or lower left only) ; default 'lower'
        grid       -- (boolean) True to add a grid to the plot ; default True
    
    """
    plt.matshow(dataarray, fignum=None, origin=p_origin, aspect='auto', cmap=colormap)
    plt.colorbar()
    plt.title(p_name, loc='center', position=(0.5,-0.2))
    
    yticksnbr=int(((vmax-vmin)/0.05)+1)
    y_positions=np.arange(0, yticksnbr, int(vres/0.05))
    y_labels=np.arange(vmin, vmax+0.1, vres)
    #for i in range(len(y_labels)): y_labels[i]=round(y_labels[i], 2)
    plt.yticks(y_positions, y_labels)
    plt.ylabel('velocity [km.s-1]')
    
    x_positions=np.arange(0, abs(posmin)+abs(posmax)+1, posres)
    x_labels=np.arange(posmin, posmax+1, posres)
    plt.xticks(x_positions, x_labels)
    plt.xlabel(p_axis+' position')
        
    if grid==True: 
        plt.grid(which='both', axis='both')
    
    plt.savefig(fname=savedir+p_name+".jpg", dpi=200, quality=95, bbox_inches='tight')


#==============================================================================


def spectre(p_vabs, p_Inu, p_title, p_filename, savedir='./', p_ymax=5e-6, grid=True):
    """Function used to plot and save simple emission spectra.
    
    Inputs :
        p_vabs      -- 1D array containing the horizontal axis' values (velocity axis)
        p_Inu       -- 1D array containing values of the spectre (intensities)
        p_title     -- (string) title of the plot
        p_filename  -- (string) name of the file (without extension)
        savedir     -- (string) path where the histogram will be saved ; default './'
        p_ymax      -- maximum value of the vertical axis (intensity). It enable to get a same vertical axis for a bunch of spectre, which is useful to compare them ; default 5e-6   [K]
        grid       -- (boolean) True to add a grid to the plot ; default True
        
    """
    fig1 = plt.figure()
    fig1.subplots_adjust(top=1)
    ax1 = fig1.add_subplot(111)
    ax1.set_ylabel('I(v)  [K]')
    ax1.set_xlabel('v  [km.s-1]')
    ax1.set_xlim(np.amin(p_vabs), np.amax(p_vabs))
    ax1.set_ylim(0, p_ymax*1.05)
    ax1.set_title(p_title)
    ax1.plot(p_vabs, p_Inu, color='blue', lw=1)#, label=p_label)
    #plt.legend(loc='upper right')
    if grid==True: plt.grid()
    plt.savefig(fname=savedir+p_filename+'.jpg', dpi=100, quality=45, bbox_inches='tight')


#==============================================================================


def histogram(p_data, p_name, p_units, log=False, ymax=None, xmin=None, xmax=None, p_bins=100, savedir='./'):
    """Function used to plot and save histograms.
    
    Inputs :
        p_data   -- data 
        p_name   -- (string) name of the file (without extension) and title of the plot
        p_units  -- (string) units of the quantity
        log      -- (boolean) True to set the horizontal axis in log10 ; default False
        ymax     -- maximum value of the vertical axis ; default None
        xmin     -- minimum value of the horizontal axis ; default None
        xmax     -- maximum value of the horizontal axis ; default None
        p_bins   -- number of bins of the histogram ; default 100
        savedir  -- (string) path where the histogram will be saved ; default './'
    
    """
    fig1 = plt.figure()
    fig1.subplots_adjust(top=1)
    ax1 = fig1.add_subplot(111)    
    filename='histogram_'+p_name
    xtitle=p_name+'_'+p_units
    if log==True:
        p_data=np.log10(p_data)
        filename='histogram_log10_'+p_name
        xtitle='log10_'+p_name+'_'+p_units
    if ymax is not None:
        ax1.set_ylim(0, ymax)
    if xmin is not None and xmax is not None:
        ax1.set_xlim(xmin, xmax)
    ax1.set_title(filename+'_'+str(p_bins)+'_bins')
    ax1.set_xlabel(xtitle)
    p_data=p_data.flatten()
    plt.hist(p_data, bins=p_bins)
    plt.grid()
    plt.savefig(fname=savedir+filename+'.jpg', dpi=200, quality=95, bbox_inches='tight')    
    

#==============================================================================


def vfield(p_u1, p_u2, p_U2, p_interv, name, units, xlab, ylab, posmin=-30, posmax=30, p_scale=2, savedir='./'):
    """Function used to plot and save vector fields / maps of vectorial quantities (e.g. velocity, magnetic field).
    
    Inputs :
        p_u1      -- 2D array containing the horizontal axis components
        p_u2      -- 2D array containing the vertical axis components
        p_U2      -- 2D array containing the squared norm of the vectors
        p_interv  -- (integer) interval between 2 points where a vector is drawn
        name      -- (string) name of the file (without extension) and title of the plot
        units     -- (string) units of the quantity
        xlab      -- (string) label of the horizontal axis
        ylab      -- (string) label of the vertical axis
        posmin    -- (integer) minimum value of the horiz/vertic axis ; default -30
        posmax    -- (integer) maximum value of the horiz/vertic axis ; default  30
        p_scale   -- (float) smaller scale -> longer arrow ; default 2
        savedir   -- (string) path where the plot will be saved ; default './'
        
    """
    dim=len(p_u1)
    nbrpts=int(dim/p_interv)+1
    grid=np.arange(0, dim+1, p_interv)
    u1_field=np.zeros((nbrpts, nbrpts))
    u2_field=np.zeros_like(u1_field)
    u_color=np.zeros_like(u1_field)
    
    for i in range(nbrpts):
        for j in range(nbrpts):
            x=grid[i]
            y=grid[j]
            u1_field[i,j]=p_u1[x,y]
            u2_field[i,j]=p_u2[x,y]
            u_color[i,j]=np.sqrt(p_U2[x,y])
    
    tickspos=np.arange(0, nbrpts, 2)
    tickslab=np.zeros_like(tickspos)
    ticksint=(posmax-posmin)/(len(tickspos)-1)
    for i in range(len(tickspos)): tickslab[i]=str((i*int(ticksint))+posmin)
    
    fig1 = plt.figure()
    fig1.subplots_adjust(top=1)
    ax1 = fig1.add_subplot(111)
    title=name+' '+units
    ax1.set_title(title)
    ax1.set_xlabel(xlab)
    ax1.set_ylabel(ylab)
    plt.xticks(tickspos, tickslab)
    plt.yticks(tickspos, tickslab)
    quiver = ax1.quiver(np.arange(nbrpts), np.arange(nbrpts), u1_field, u2_field, u_color, units='inches', scale=p_scale, cmap='jet')
    plt.colorbar(quiver)
    plt.savefig(fname=savedir+name+'.jpg', dpi=200, quality=95, bbox_inches='tight')

