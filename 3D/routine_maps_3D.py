#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 24 09:49:31 2019

@author: gontelm
"""

import numpy as np
import matplotlib.pyplot as plt
import os
import plotting_3D
import read_data_3D



def hist_and_maps(struct, axisarray, planenbrarray, nhmin, xco, savedir='./', histog=True, maps=True, fields=True):
    """Function plotting and saving histograms, colormaps (and vector fields) of physical quantities from 3D data cubes.
    
    Inputs :
        struct        -- (string) name of the structure, must be the same than the .vtk file (e.g. 'shock_slow' for the 'shock_slow.vtk')
        axisarray     -- 1D array indicating along wich axis ('x', 'y' or 'z') plotting the colormaps and vector fields
        planenbrarray -- 1D array indicating on which plane of the cube (along the axis indicating in axisarray) the colormaps end vector fields will be plot
        nhmin         -- threshold : minimum value of the density [cm-3]
        xco           -- abundance of CO when the prescription is fulfilled [-]
        savedir       -- (string) path where to save the .fits spectra data files ; default './'
        histog        -- (boolean) True to compute histograms of the physical quantities ; default True
        maps          -- (boolean) True to compute colormaps of the physical quantities ; default True
        fields        -- (boolean) True to compute vector fields of the velocity and the magnetic field ; default True
        
    """
    
    # Creation of a directory to save the .fits files
    filepath=savedir+struct+'/'
    if os.path.exists(filepath) == False:
        os.mkdir(filepath)

    
    dat=read_data_3D.read_data(struct)
    rho=dat[0]
    nh=dat[1]
    ntot=dat[2]
    vx=dat[3]
    vy=dat[4]
    vz=dat[5]
    v2=dat[6]
    Bx=dat[7]
    By=dat[8]
    Bz=dat[9]
    B2=dat[10]
    pth=dat[11]
    pdyn=dat[12]
    pmag=dat[13]
    
    #ptot=pdyn+pmag
    
    nco=read_data_3D.prescription_CO(nh, nhmin, xco)
    
    
    if histog==True:
        plotting_3D.histogram(nh, "n_H", "[cm-3]", True, 100, savedir=filepath)
        plotting_3D.histogram(vx, "v_x", "[km.s-1]", False, 100, savedir=filepath)
        plotting_3D.histogram(vy, "v_y", "[km.s-1]", False, 100, savedir=filepath)
        plotting_3D.histogram(vz, "v_z", "[km.s-1]", False, 100, savedir=filepath)
        plotting_3D.histogram(v2, "v_2", "[km2.s-2]", True, 100, savedir=filepath)
        plotting_3D.histogram(pth, "P_th", "[K.cm-3]", True, 100, savedir=filepath)
        plotting_3D.histogram(pdyn, "P_dyn", "[K.cm-3]", True, 100, savedir=filepath)
        plotting_3D.histogram(pmag, "P_mag", "[K.cm-3]", False, 100, savedir=filepath)
        plotting_3D.histogram(pmag, "P_mag", "[K.cm-3]", True, 100, savedir=filepath)
        #plotting_3D.histogram(ptot, "P_tot", "[K.cm-3]", True, 100, savedir=filepath)
        plotting_3D.histogram(Bx, "B_x", "[microGauss]", False, 100, savedir=filepath)
        plotting_3D.histogram(By, "B_y", "[microGauss]", False, 100, savedir=filepath)
        plotting_3D.histogram(Bz, "B_z", "[microGauss]", False, 100, savedir=filepath)
        plotting_3D.histogram(B2, "B_2", "[microGauss2]", False, 100, savedir=filepath)
               
        plotting_3D.histogram(nco, 'n_CO', '[cm-3]', False, ymax=300, xmin=0.05, xmax=0.2, p_bins=100, savedir=filepath)
    
    
            
    for axis in axisarray:
        for planenbr in planenbrarray:
        
            if axis=='z':
                
                if maps==True:
                    plotting_3D.datamap(np.log10(nh[planenbr]), "log10_n_H_z_"+str(planenbr), "[cm-3]", 'x position', 'y position', savedir=filepath)
                    plotting_3D.datamap(B2[planenbr], "B_2_z_"+str(planenbr), "[microGauss2]", 'x position', 'y position', savedir=filepath)
                    plotting_3D.datamap(v2[planenbr], "v_2_z_"+str(planenbr), "[km2.s-2]", 'x position', 'y position', savedir=filepath)
                    plotting_3D.datamap(vz[planenbr], "v_z_z_"+str(planenbr), "[km.s-1]", 'x position', 'y position', savedir=filepath)
                    #plotting_3D.datamap(rho[planenbr], "rho_z_"+str(planenbr), "[g.cm-3]", 'x position', 'y position', savedir=filepath)
                    plotting_3D.datamap(pth[planenbr], "P_th_z_"+str(planenbr), "[K.cm-3]", 'x position', 'y position', savedir=filepath)
                    plotting_3D.datamap(pdyn[planenbr], "P_dyn_z_"+str(planenbr), "[K.cm-3]", 'x position', 'y position', savedir=filepath)
                    plotting_3D.datamap(pmag[planenbr], "P_mag_z_"+str(planenbr), "[K.cm-3]", 'x position', 'y position', savedir=filepath)
                    #plotting_3D.datamap(ptot[planenbr], "P_tot_z_"+str(planenbr), "[K.cm-3]", 'x position', 'y position', savedir=filepath)
                    plotting_3D.datamap(np.log10(nco[planenbr]), "log10_n_CO_z_"+str(planenbr), "[cm-3]", 'x position', 'y position', savedir=filepath)
                
                if fields==True: 
                    v2proj=(vx[planenbr])**2+(vy[planenbr])**2
                    plotting_3D.vfield(vx[planenbr], vy[planenbr], v2proj, 3, 'velocity_field_z_'+str(planenbr), '[km.s-1]', 'x position', 'y position', savedir=filepath)
                    B2proj=(Bx[planenbr])**2+(By[planenbr])**2
                    plotting_3D.vfield(Bx[planenbr], By[planenbr], B2proj, 3, 'magnetic_field_z_'+str(planenbr), '[microGauss]', 'x position', 'y position', p_scale=25, savedir=filepath)

            
            if axis=='y':
                
                if maps==True:
                    plotting_3D.datamap(np.log10(nh[:,planenbr,:]), "log10_n_H_y_"+str(planenbr), "[cm-3]", 'x position', 'z position', savedir=filepath)
                    plotting_3D.datamap(B2[:,planenbr,:], "B_2_y_"+str(planenbr), "[microGauss2]", 'x position', 'z position', savedir=filepath)
                    plotting_3D.datamap(v2[:,planenbr,:], "v_2_y_"+str(planenbr), "[km2.s-2]", 'x position', 'z position', savedir=filepath)
                    plotting_3D.datamap(vy[:,planenbr,:], "v_y_y_"+str(planenbr), "[km.s-1]", 'x position', 'z position', savedir=filepath)
                    #plotting_3D.datamap(rho[:,planenbr,:], "rho_y_"+str(planenbr), "[g.cm-3]", 'x position', 'z position', savedir=filepath)
                    plotting_3D.datamap(pth[:,planenbr,:], "P_th_y_"+str(planenbr), "[K.cm-3]", 'x position', 'z position', savedir=filepath)
                    plotting_3D.datamap(pdyn[:,planenbr,:], "P_dyn_y_"+str(planenbr), "[K.cm-3]", 'x position', 'z position', savedir=filepath)
                    plotting_3D.datamap(pmag[:,planenbr,:], "P_mag_y_"+str(planenbr), "[K.cm-3]", 'x position', 'z position', savedir=filepath)
                    #plotting_3D.datamap(ptot[:,planenbr,:], "P_tot_y_"+str(planenbr), "[K.cm-3]", 'x position', 'z position', savedir=filepath)
                    plotting_3D.datamap(np.log10(nco[:,planenbr,:]), "log10_n_CO_y_"+str(planenbr), "[cm-3]", 'x position', 'z position', savedir=filepath)
                
                if fields==True:
                    v2proj=(vx[:,planenbr,:])**2+(vz[:,planenbr,:])**2
                    plotting_3D.vfield(vx[:,planenbr,:], vz[:,planenbr,:], v2proj, 3, 'velocity_field_y_'+str(planenbr), '[km.s-1]', 'x position', 'z position', savedir=filepath)
                    B2proj=(Bx[:,planenbr,:])**2+(Bz[:,planenbr,:])**2
                    plotting_3D.vfield(Bx[:,planenbr,:], Bz[:,planenbr,:], B2proj, 3, 'magnetic_field_y_'+str(planenbr), '[microGauss]', 'x position', 'z position', p_scale=25, savedir=filepath)
            
            
            if axis=='x':
                
                if maps==True:
                    plotting_3D.datamap(np.log10(nh[:,:,planenbr]), "log10_n_H_x_"+str(planenbr), "[cm-3]", 'y position', 'z position', savedir=filepath)
                    plotting_3D.datamap(B2[:,:,planenbr], "B_2_x_"+str(planenbr), "[microGauss2]", 'y position', 'z position', savedir=filepath)
                    plotting_3D.datamap(v2[:,:,planenbr], "v_2_x_"+str(planenbr), "[km2.s-2]", 'y position', 'z position', savedir=filepath)
                    plotting_3D.datamap(vx[:,:,planenbr], "v_x_x_"+str(planenbr), "[km.s-1]", 'y position', 'z position', savedir=filepath)
                    #plotting_3D.datamap(rho[:,:,planenbr], "rho_x_"+str(planenbr), "[g.cm-3]", 'y position', 'z position', savedir=filepath)
                    plotting_3D.datamap(pth[:,:,planenbr], "P_th_x_"+str(planenbr), "[K.cm-3]", 'y position', 'z position', savedir=filepath)
                    plotting_3D.datamap(pdyn[:,:,planenbr], "P_dyn_x_"+str(planenbr), "[K.cm-3]", 'y position', 'z position', savedir=filepath)
                    plotting_3D.datamap(pmag[:,:,planenbr], "P_mag_x_"+str(planenbr), "[K.cm-3]", 'y position', 'z position', savedir=filepath)
                    #plotting_3D.datamap(ptot[:,:,planenbr], "P_tot_x_"+str(planenbr), "[K.cm-3]", 'y position', 'z position', savedir=filepath)
                    plotting_3D.datamap(np.log10(nco[:,:,planenbr]), "log10_n_CO_x_"+str(planenbr), "[cm-3]", 'y position', 'z position', savedir=filepath)
                
                if fields==True:
                    v2proj=(vy[:,:,planenbr])**2+(vz[:,:,planenbr])**2
                    plotting_3D.vfield(vy[:,:,planenbr], vz[:,:,planenbr], v2proj, 3, 'velocity_field_x_'+str(planenbr), '[km.s-1]', 'y position', 'z position', savedir=filepath)
                    B2proj=(By[:,:,planenbr])**2+(Bz[:,:,planenbr])**2
                    plotting_3D.vfield(By[:,:,planenbr], Bz[:,:,planenbr], B2proj, 3, 'magnetic_field_x_'+str(planenbr), '[microGauss]', 'y position', 'z position', p_scale=25, savedir=filepath)
        



#%%




structure='current_sheet'
#structure='shock_slow'
#structure='shock_fast'

axisarray=['x', 'y', 'z']
planenbrarray=[0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60]


xco=1e-4    # [-] prescription abundance of CO
nhmin=300   # [cm-3] prescription threshold on n_H

hist_and_maps(structure, axisarray, planenbrarray, nhmin, xco, savedir='./test/', histog=False)





#%%



