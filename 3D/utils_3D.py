#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 28 14:44:53 2019

@author: gontelm
"""

from vtk import *
#import re
from numpy import shape,sum



def read_dat_vtk(filename):
    """Read vtk 3D files.
    
    Input : 
        filename -- (string) path and name of the vtk file (e.g. './vtk/file.vtk')
    
    Output : 
        dat -- dictionary containing data arrays with their name
    """
    
    import vtk.util.numpy_support
    v2n=util.numpy_support.vtk_to_numpy

    Reader = vtkRectilinearGridReader()
    Reader.SetFileName(filename)
    Reader.SetReadAllScalars(True)
    Reader.Update()
    data=Reader.GetOutput()
    #print('Reader')
    #print(Reader)
    #print('data')
    #print(data)
    dim=data.GetDimensions()
    #print('dimensions:',dim)
    cd=data.GetCellData()
    #print('celldata')
    #print(cd)
    #print('nbr of arrays :', cd.GetNumberOfArrays())
    dat={}
    for i in range(cd.GetNumberOfArrays()):
        name=cd.GetArrayName(i)
        #dat[name]=v2n(data.GetCellData().GetArray(name)).reshape((dim[1],dim[0],dim[2]))
        dat[name]=v2n(data.GetCellData().GetArray(name)).reshape((dim[0]-1,dim[0]-1,dim[0]-1))
    return dat



## Read single variable in a vtk file
#def read_single(file, var): 
#    # if re.search('M0',file):
#    #     dat1=read_giorgos_vtk(file)
#    # else:
#    #     dat1=read_dat_vtk(file)
#    dat1=read_dat_vtk(file)
#    #print(dat1)
#    #vars=dat1.keys()
#    #print(vars)
#    #var=list(vars)[0]
#    #print(var)
#    a=dat1[var]
#    #print(a)
#    #print(shape(a))
#    #if shape(a)[2]>1:
#    #   a=sum(a,axis=0)
#    #else:
#    #   a=a[:,:,0]
#    #print(a)
#    #print(shape(a))
#    # Normalises projection
#    #print('Divides by ',shape(a)[0])
#    #a=a/shape(a)[0]#*float(thick)
#    return a
